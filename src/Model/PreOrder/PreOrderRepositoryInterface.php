<?php

namespace App\Model\PreOrder;

/**
 * Interface PreOrderRepositoryInterface
 * @package App\Model\PreOrder
 */
interface PreOrderRepositoryInterface
{
    /**
     * @param int $preOrderId
     * @return PreOrder
     */
    public function findById(int $preOrderId): ?PreOrder;

    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param PreOrder $preOrder
     */
    public function save(PreOrder $preOrder): void;

    /**
     * @param PreOrder $preOrder
     */
    public function delete(PreOrder $preOrder): void;

    /**
     * @return PreOrder|null
     */
    public function autoRejectedPreOrders(): ?array;
}