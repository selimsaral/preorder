<?php

namespace App\Model\PreOrder;

class PreOrderStatus
{
    const STATUS_WAITING = "waiting";
    const STATUS_AUTOREJECTED = "autoRejected";
    const STATUS_APPROVED = "approved";
    const STATUS_REJECTED = "rejected";
}