<?php

namespace App\Model\PreOrderDetail;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * Class PreOrderDetail
 * @ORM\Entity
 * @package App\Model\PreOrderDetail
 */
class PreOrderDetail
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Model\PreOrder\PreOrder", inversedBy="detail" , cascade="persist")
     * @ORM\JoinColumn(name="pre_order_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    protected $pre_order_id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $product_name;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $product_price;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $quantity;

    /**
     * @return mixed
     */
    public function getPreOrderId()
    {
        return $this->pre_order_id;
    }

    /**
     * @param mixed $pre_order_id
     */
    public function setPreOrderId($pre_order_id): void
    {
        $this->pre_order_id = $pre_order_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->product_name;
    }

    /**
     * @param mixed $product_name
     */
    public function setProductName($product_name): void
    {
        $this->product_name = $product_name;
    }

    /**
     * @return mixed
     */
    public function getProductPrice()
    {
        return $this->product_price;
    }

    /**
     * @param mixed $product_price
     */
    public function setProductPrice($product_price): void
    {
        $this->product_price = $product_price;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }
}