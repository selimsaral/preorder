<?php

namespace App\Model\PreOrderDetail;

/**
 * Interface PreOrderDetailRepositoryInterface
 * @package App\Model\PreOrder
 */
interface PreOrderDetailRepositoryInterface
{
    /**
     * @param int $preOrderDetailId
     * @return PreOrderDetail|null
     */
    public function findById(int $preOrderDetailId): ?PreOrderDetail;

    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param PreOrderDetail $preOrder
     */
    public function save(PreOrderDetail $preOrder): void;

    /**
     * @param PreOrderDetail $preOrder
     */
    public function delete(PreOrderDetail $preOrder): void;
}