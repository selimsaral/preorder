<?php

namespace App\Model\Basket;

/**
 * Interface BasketRepositoryInterface
 * @package App\Model\Basket
 */
interface BasketRepositoryInterface
{
    /**
     * @param string $sessionId
     * @return Basket|null
     */
    public function getBasket(string $sessionId): array;

    /**
     * @param array $basket
     * @return mixed
     */
    public function clearBasket(array $basket);

    /**
     * @param Basket $basket
     */
    public function addItem(Basket $basket): void;

    /**
     * @param Basket $basket
     */
    public function save(Basket $basket): void;

    /**
     * @param int $basketId
     * @return Basket|null
     */
    public function findById(int $basketId): ?Basket;

    /**
     * @param Basket $basket
     */
    public function delete(Basket $basket): void;
}