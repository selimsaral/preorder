<?php

namespace App\Model\Products;

class Products
{
    const PRODUCTS = [
        0 => [
            "product_name" => "Test Ürünü",
            "price"        => 100
        ],
        1 => [
            "product_name" => "Test Ürünü 2",
            "price"        => 150
        ],
        2 => [
            "product_name" => "Test Ürünü 3",
            "price"        => 200
        ]
    ];
}