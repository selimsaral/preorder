<?php

namespace App\Command;

use App\Model\PreOrder\PreOrderStatus;
use App\Service\PreOrderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

class AutoRejected extends Command
{
    protected static $defaultName = 'preorder:autorejected';

    private $preOrderService;

    public function __construct(PreOrderService $preOrderService)
    {
        $this->preOrderService = $preOrderService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Auto Rejected Command');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $preOrders = $this->preOrderService->AutoRejectedPreOrders();
        foreach ($preOrders as $preOrder) {
            $this->preOrderService->changeStatusPreOrder($preOrder->getId(), PreOrderStatus::STATUS_AUTOREJECTED);

            $output->writeln($preOrder->getId() . ' PreOrder Set Status AutoRejected');
        }


    }
}