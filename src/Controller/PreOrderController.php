<?php

namespace App\Controller;

use App\DTO\PreOrderDTO;
use App\Service\PreOrderService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/**
 * Class PreOrderController
 * @package App\Controller
 */
class PreOrderController extends FOSRestController
{
    /**
     * @var PreOrderService
     */
    private $preOrderService;

    /**
     * PreOrderController constructor.
     * @param PreOrderService $preOrderService
     */
    public function __construct(PreOrderService $preOrderService)
    {
        $this->preOrderService = $preOrderService;
    }

    /**
     * Retrieves an PreOrder resource
     * @Rest\Get("/preorder/{preOrderId}")
     * @param int $preOrderId
     * @return View
     */
    public function getPreOrder(int $preOrderId): View
    {
        $preOrder = $this->preOrderService->getPreOrder($preOrderId);

        return View::create($preOrder, Response::HTTP_OK);
    }

    /**
     * Retrieves a collection of PreOrder resource
     * @Rest\Get("/preorders")
     * @rest\View(populateDefaultVars=false)
     * @return View
     */
    public function getPreOrders()
    {
        $preOrders = $this->preOrderService->getAllPreOrders();

        return View::create($preOrders, Response::HTTP_OK);
    }

    /**
     * Creates an PreOrder resource
     * @Rest\Post("/preorder")
     * @ParamConverter("preOrderDTO", converter="fos_rest.request_body")
     * @param PreOrderDTO $preOrderDTO
     * @param SessionInterface $session
     * @return View
     */
    public function postPreOrder(PreOrderDTO $preOrderDTO, SessionInterface $session)
    {
        $preOrder = $this->preOrderService->addPreOrder($preOrderDTO, $session->getId());

        return View::create($preOrder, Response::HTTP_CREATED);
    }

    /**
     * Replaces PreOrder resource
     * @Rest\Put("/preorder/{preOrderId}")
     * @ParamConverter("preOrderDTO", converter="fos_rest.request_body")
     * @param int $preOrderId
     * @param PreOrderDTO $preOrderDTO
     * @return View
     */
    public function putPreOrder(int $preOrderId, PreOrderDTO $preOrderDTO)
    {
        $preOrder = $this->preOrderService->updatePreOrder($preOrderId, $preOrderDTO);

        return View::create($preOrder, Response::HTTP_OK);
    }


    /**
     * Removes the PreOrder resource
     * @Rest\Delete("/preorder/{preOrderId}")
     * @param int $preOrderId
     * @return View
     */
    public function deletePreOrder(int $preOrderId): View
    {
        $this->preOrderService->deletePreOrder($preOrderId);

        return View::create([], Response::HTTP_NO_CONTENT);
    }


    /**
     * Replaces PreOrder Status
     * @Rest\Put("/preorder/updateStatus/{preOrderId}")
     * @param int $preOrderId
     * @param Request $request
     * @IsGranted("ROLE_ADMIN")
     * @return View
     */
    public function changeStatus(int $preOrderId, Request $request)
    {
        $this->preOrderService->changeStatusPreOrder($preOrderId, $request->get('status'));

        return View::create([], Response::HTTP_OK);
    }
}