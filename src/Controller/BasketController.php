<?php

namespace App\Controller;

use App\DTO\BasketDTO;
use App\Service\BasketService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class BasketController
 * @package App\Controller
 */
class BasketController extends FOSRestController
{
    /**
     * @var BasketService
     */
    private $basketService;

    /**
     * BasketController constructor.
     * @param BasketService $basketService
     */
    public function __construct(BasketService $basketService)
    {
        $this->basketService = $basketService;
    }

    /**
     * Retrieves a collection of Basket resource
     * @Rest\Get("/basket")
     * @param SessionInterface $session
     * @return View
     */
    public function getBasket(SessionInterface $session)
    {
        $basket = $this->basketService->getBasket($session->getId());

        return View::create($basket, Response::HTTP_OK);
    }

    /**
     * Create an Basket resource
     * @Rest\Post("/basket")
     * @ParamConverter("basketDTO", converter="fos_rest.request_body")
     * @param BasketDTO $basketDTO
     * @param SessionInterface $session
     * @return View
     */
    public function postBasket(BasketDTO $basketDTO)
    {
        $session = new Session();

        $preOrder = $this->basketService->addBasket($basketDTO, $session->getId());

        return View::create($preOrder, Response::HTTP_CREATED);
    }


    /**
     * Replaces Basket resource
     * @Rest\Put("/basket/{basketId}")
     * @ParamConverter("basketDTO", converter="fos_rest.request_body")
     * @param int $basketId
     * @param BasketDTO $basketDTO
     * @param SessionInterface $session
     * @return View
     */
    public function putPreOrder(int $basketId, basketDTO $basketDTO, SessionInterface $session)
    {
        $preOrder = $this->basketService->updateBasket($basketId, $basketDTO, $session->getId());

        return View::create($preOrder, Response::HTTP_OK);
    }

    /**
     * Removes the Basket resource
     * @Rest\Delete("/basket/{basketId}")
     * @param int $basketId
     * @return View
     */
    public function deletePreOrder(int $basketId): View
    {
        $this->basketService->deleteBasketItem($basketId);

        return View::create([], Response::HTTP_NO_CONTENT);
    }

}