<?php

namespace App\DTO;

use App\Model\Basket\Basket;
use App\Model\PreOrder\PreOrder;
use App\Model\PreOrder\PreOrderStatus;
use App\Model\PreOrderDetail\PreOrderDetail;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class PreOrderAssembler
 * @package App\DTO
 */
final class PreOrderAssembler
{
    /**
     * @param PreOrderDTO $preOrderDTO
     * @param array|null $basket
     * @param PreOrder|null $preOrder
     * @return PreOrder
     * @throws \Exception
     */
    public function readDTO(PreOrderDTO $preOrderDTO, ?array $basket, ?PreOrder $preOrder = null): PreOrder
    {
        if (!$preOrder) {
            $preOrder = new PreOrder();

            $preOrder->setStatus(PreOrderStatus::STATUS_WAITING);

            $datetime = new DateTime();
            $datetime->modify('+1 day');

            $preOrder->setExpireAt(new DateTime($datetime->format('Y-m-d H:i:s')));
        }

        $preOrder->setName($preOrderDTO->getName());
        $preOrder->setSurname($preOrderDTO->getSurname());
        $preOrder->setEmail($preOrderDTO->getEmail());
        $preOrder->setPhone($preOrderDTO->getPhone());


        $preOrderDetail = new ArrayCollection();

        foreach ($basket as $key => $item) {

            $preDetail = new PreOrderDetail();
            $preDetail->setPreOrderId($preOrder);
            $preDetail->setProductName($item->getProductName());
            $preDetail->setProductPrice($item->getProductPrice());
            $preDetail->setQuantity($item->getQuantity());

            $preOrderDetail->set($key, $preDetail);
        }

        $preOrder->setDetail($preOrderDetail);

        return $preOrder;
    }

    /**
     * @param PreOrderDTO $preOrderDTO
     * @param array $basket
     * @return PreOrder
     * @throws \Exception
     */
    public function createPreOrder(PreOrderDTO $preOrderDTO, array $basket): PreOrder
    {
        return $this->readDTO($preOrderDTO, $basket);
    }

    /**
     * @param PreOrder $preOrder
     * @param PreOrderDTO $preOrderDTO
     * @param array $basket
     * @return PreOrder
     * @throws \Exception
     */
    public function updatePreOrder(PreOrder $preOrder, PreOrderDTO $preOrderDTO, array $basket): PreOrder
    {
        return $this->readDTO($preOrderDTO, $basket, $preOrder);
    }

    /**
     * @param PreOrder $preOrder
     * @return PreOrderDTO
     */
    public function writeDTO(PreOrder $preOrder)
    {
        return new PreOrderDTO(
            $preOrder->getName(),
            $preOrder->getSurname(),
            $preOrder->getEmail(),
            $preOrder->getPhone()
        );
    }
}