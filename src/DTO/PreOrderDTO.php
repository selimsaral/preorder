<?php

namespace App\DTO;

use App\Model\PreOrder\PreOrderStatus;
use Doctrine\Common\Collections\ArrayCollection;

final class PreOrderDTO
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var ArrayCollection
     */
    private $detail;

    /**
     * PreOrderDTO constructor.
     * @param string $name
     * @param string $surname
     * @param string $email
     * @param string $phone
     * @param string|null $status
     */
    public function __construct(string $name, string $surname, string $email, string $phone)
    {
        $this->name    = $name;
        $this->surname = $surname;
        $this->email   = $email;
        $this->phone   = $phone;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return ArrayCollection
     */
    public function getDetail(): ArrayCollection
    {
        return $this->detail;
    }
}