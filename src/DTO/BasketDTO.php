<?php

namespace App\DTO;

/**
 * Class BasketDTO
 */
final class BasketDTO
{
    /**
     * @var integer
     */
    private $product_id;

    /**
     * @var int
     */
    private $quantity;

    /**
     * BasketDTO constructor.
     * @param int $product_id
     * @param int $quantity
     */
    public function __construct(int $product_id, int $quantity)
    {
        $this->product_id = $product_id;
        $this->quantity   = $quantity;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->product_id;
    }

}