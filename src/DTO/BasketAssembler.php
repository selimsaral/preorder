<?php

namespace App\DTO;

use App\Model\Basket\Basket;
use App\Model\Products\Products;

/**
 * Class BasketAssembler
 * @package App\DTO
 */
final class BasketAssembler
{
    /**
     * @param BasketDTO $basketDTO
     * @param string $sessionId
     * @param Basket|null $basket
     * @return Basket
     */
    public function readDTO(BasketDTO $basketDTO, string $sessionId, ?Basket $basket = null): Basket
    {
        if (!$basket) {
            $basket = new Basket();
        }

        $product = Products::PRODUCTS[$basketDTO->getProductId()] ?? Products::PRODUCTS[0];

        $basket->setSessionId($sessionId);
        $basket->setProductName($product['product_name']);
        $basket->setProductPrice($product['price']);
        $basket->setQuantity($basketDTO->getQuantity());

        return $basket;
    }

    /**
     * @param BasketDTO $basketDTO
     * @param string $sessionId
     * @return Basket
     */
    public function createBasket(BasketDTO $basketDTO, string $sessionId): Basket
    {
        return $this->readDTO($basketDTO, $sessionId);
    }

    /**
     * @param Basket $basket
     * @param BasketDTO $basketDTO
     * @param string $sessionId
     * @return Basket
     */
    public function updateBasket(Basket $basket, BasketDTO $basketDTO, string $sessionId): Basket
    {
        return $this->readDTO($basketDTO, $sessionId, $basket);
    }

    /**
     * @param Basket $basket
     * @return BasketDTO
     */
    public function writeDTO(Basket $basket)
    {
        return new BasketDTO(
            $basket->getProductName(),
            $basket->getProductPrice(),
            $basket->getQuantity()
        );
    }
}