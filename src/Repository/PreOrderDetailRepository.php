<?php

namespace App\Repository;

use App\Model\PreOrderDetail\PreOrderDetail;
use App\Model\PreOrderDetail\PreOrderDetailRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

final class PreOrderDetailRepository implements PreOrderDetailRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $objectRepository;

    /**
     * PreOrderDetailRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager    = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(PreOrderDetail::class);
    }

    /**
     * @param int $preOrderDetailId
     * @return PreOrderDetail|null
     */
    public function findById(int $preOrderDetailId): ?PreOrderDetail
    {
        return $this->objectRepository->find($preOrderDetailId);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->objectRepository->findAll();
    }

    /**
     * @param PreOrderDetail $preOrderDetail
     */
    public function save(PreOrderDetail $preOrderDetail): void
    {
        $this->entityManager->persist($preOrderDetail);
        $this->entityManager->flush();
    }

    /**
     * @param PreOrderDetail $preOrderDetail
     */
    public function delete(PreOrderDetail $preOrderDetail): void
    {
        $this->entityManager->remove($preOrderDetail);
        $this->entityManager->flush();
    }
}