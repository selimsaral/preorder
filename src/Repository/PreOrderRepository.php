<?php

namespace App\Repository;

use App\Model\PreOrder\PreOrder;
use App\Model\PreOrder\PreOrderRepositoryInterface;
use App\Model\PreOrder\PreOrderStatus;
use Doctrine\ORM\EntityManagerInterface;

final class PreOrderRepository implements PreOrderRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $objectRepository;

    /**
     * PreOrderRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager    = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(PreOrder::class);
    }

    /**
     * @param int $preOrderId
     * @return \App\Model\PreOrder\PreOrder
     */
    public function findById(int $preOrderId): ?PreOrder
    {
        return $this->objectRepository->find($preOrderId);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->objectRepository->findAll();
    }

    /**
     * @param \App\Model\PreOrder\PreOrder $preOrder
     */
    public function save(PreOrder $preOrder): void
    {
        $this->entityManager->persist($preOrder);
        $this->entityManager->flush();
    }

    /**
     * @param \App\Model\PreOrder\PreOrder $preOrder
     */
    public function delete(PreOrder $preOrder): void
    {
        $this->entityManager->remove($preOrder);
        $this->entityManager->flush();
    }

    /**
     * @return PreOrder|null
     */
    public function autoRejectedPreOrders(): ?array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('e')
            ->from('App\Model\PreOrder\PreOrder', 'e')
            ->where('e.expire_at <= :now')->andWhere('e.status = :status')
            ->setParameter('status', PreOrderStatus::STATUS_WAITING)
            ->setParameter('now', date('Y-m-d H:i:S'))
            ->getQuery()
            ->getResult();
    }
}