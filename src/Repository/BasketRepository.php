<?php

namespace App\Repository;

use App\Model\Basket\Basket;
use App\Model\Basket\BasketRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

final class BasketRepository implements BasketRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $objectRepository;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager    = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Basket::class);
    }

    /**
     * @param string $sessionId
     * @return array
     */
    public function getBasket(string $sessionId): array
    {
        return $this->objectRepository->findBy(['session_id' => $sessionId]);
    }

    /**
     * @param array $basket
     * @return mixed|void
     */
    public function clearBasket(array $basket)
    {
        foreach ($basket as $item) {
            $this->entityManager->remove($item);
            $this->entityManager->flush();
        }
    }

    /**
     * @param Basket $basket
     */
    public function addItem(Basket $basket): void
    {
        $this->entityManager->persist($basket);
        $this->entityManager->flush();
    }

    /**
     * @param Basket $basket
     */
    public function save(Basket $basket): void
    {
        $this->entityManager->persist($basket);
        $this->entityManager->flush();
    }

    /**
     * @param int $basketId
     * @return Basket|null
     */
    public function findById(int $basketId): ?Basket
    {
        return $this->objectRepository->find($basketId);
    }

    /**
     * @param Basket $basket
     */
    public function delete(Basket $basket): void
    {
        $this->entityManager->remove($basket);
        $this->entityManager->flush();
    }
}