<?php

namespace App\Service;

use App\DTO\PreOrderAssembler;
use App\DTO\PreOrderDTO;
use App\Model\Basket\BasketRepositoryInterface;
use App\Model\PreOrder\PreOrder;
use App\Model\PreOrder\PreOrderRepositoryInterface;
use App\Model\PreOrder\PreOrderStatus;
use Symfony\Component\HttpFoundation\Session\Session;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Rest\Client;

/**
 * Class PreOrderService
 * @package App\Service
 */
final class PreOrderService
{
    /**
     * @var PreOrderRepositoryInterface
     */
    private $preOrderRepository;

    /**
     * @var BasketRepositoryInterface
     */
    private $basketRepository;

    /**
     * @var PreOrderAssembler
     */
    private $preOrderAssembler;


    /**
     * PreOrderService constructor.
     * @param PreOrderRepositoryInterface $preOrderRepository
     * @param BasketRepositoryInterface $basketRepository
     * @param PreOrderAssembler $preOrderAssembler
     */
    public function __construct(
        PreOrderRepositoryInterface $preOrderRepository,
        BasketRepositoryInterface $basketRepository,
        PreOrderAssembler $preOrderAssembler
    ) {
        $this->preOrderRepository = $preOrderRepository;
        $this->preOrderAssembler  = $preOrderAssembler;
        $this->basketRepository   = $basketRepository;
    }

    /**
     * @param int $preOrderId
     * @return PreOrder|null
     */
    public function getPreOrder(int $preOrderId): ?PreOrder
    {
        return $this->preOrderRepository->findById($preOrderId);
    }

    /**
     * @return array|null
     */
    public function getAllPreOrders(): ?array
    {
        return $this->preOrderRepository->findAll();
    }

    /**
     * @param PreOrderDTO $preOrderDTO
     * @param string $sessionId
     * @return PreOrder
     * @throws \Exception
     */
    public function addPreOrder(PreOrderDTO $preOrderDTO, string $sessionId): PreOrder
    {
        $basket   = $this->basketRepository->getBasket($sessionId);
        $preOrder = $this->preOrderAssembler->createPreOrder($preOrderDTO, $basket);

        $this->basketRepository->clearBasket($basket);

        $this->preOrderRepository->save($preOrder);

        return $preOrder;
    }

    /**
     * @param int $preOrderId
     * @param PreOrderDTO $preOrderDTO
     * @return PreOrder
     * @throws \Exception
     */
    public function updatePreOrder(int $preOrderId, PreOrderDTO $preOrderDTO): PreOrder
    {
        $preOrder = $this->preOrderRepository->findById($preOrderId);
        if (!$preOrder) {
            return null;
        }

        $preOrder = $this->preOrderAssembler->updatePreOrder($preOrder, $preOrderDTO, array());

        $this->preOrderRepository->save($preOrder);

        return $preOrder;
    }


    /**
     * @param int $preOrderId
     */
    public function deletePreOrder(int $preOrderId): void
    {
        $preOrder = $this->preOrderRepository->findById($preOrderId);
        if ($preOrder) {
            $this->preOrderRepository->delete($preOrder);
        }
    }

    /**
     * @param int $preOrderId
     * @param string $status
     * @return bool
     */
    public function changeStatusPreOrder(int $preOrderId, string $status)
    {
        if (!in_array($status, [
            PreOrderStatus::STATUS_WAITING,
            PreOrderStatus::STATUS_APPROVED,
            PreOrderStatus::STATUS_REJECTED,
            PreOrderStatus::STATUS_AUTOREJECTED
        ])) {
            return false;
        }

        $preOrder = $this->preOrderRepository->findById($preOrderId);

        if (!$preOrder) {
            return false;
        }

        $preOrder->setStatus($status);

        $this->preOrderRepository->save($preOrder);

        try {
            $twilio = new Client("AC8450b5db3a8484df19e37fe40b31257a", "40a1e15fe4b9f63d1ee965b07c4a6c2a");

            $twilio->messages->create("+15005550010", array(
                "body" => "Test Message",
                "from" => "+15005550006"
            ));
        } catch (ConfigurationException $e) {
            return false;
        }


        return true;
    }


    public function AutoRejectedPreOrders()
    {
        return $this->preOrderRepository->autoRejectedPreOrders();
    }
}