<?php

namespace App\Service;

use App\DTO\BasketAssembler;
use App\DTO\BasketDTO;
use App\Model\Basket\Basket;
use App\Model\Basket\BasketRepositoryInterface;


/**
 * Class BasketService
 * @package App\Service
 */
final class BasketService
{
    /**
     * @var BasketRepositoryInterface
     */
    private $basketRepository;

    /**
     * @var BasketAssembler
     */
    private $basketAssembler;

    /**
     * BasketService constructor.
     * @param BasketRepositoryInterface $basketRepository
     * @param BasketAssembler $basketAssembler
     */
    public function __construct(BasketRepositoryInterface $basketRepository, BasketAssembler $basketAssembler)
    {
        $this->basketRepository = $basketRepository;
        $this->basketAssembler  = $basketAssembler;
    }

    /**
     * @param string $sessionId
     * @return \App\Model\Basket\Basket|array|null
     */
    public function getBasket(string $sessionId)
    {
        return $this->basketRepository->getBasket($sessionId);
    }

    /**
     * @param BasketDTO $basketDTO
     * @param string $sessionId
     * @return Basket
     */
    public function addBasket(BasketDTO $basketDTO, string $sessionId): Basket
    {
        $preOrder = $this->basketAssembler->createBasket($basketDTO, $sessionId);

        $this->basketRepository->addItem($preOrder);

        return $preOrder;
    }

    /**
     * @param int $basketId
     * @param BasketDTO $basketDTO
     * @param string $sessionId
     * @return Basket
     */
    public function updateBasket(int $basketId, BasketDTO $basketDTO, string $sessionId): Basket
    {
        $basket = $this->basketRepository->findById($basketId);
        if (!$basket) {
            return null;
        }

        $basket = $this->basketAssembler->updateBasket($basket, $basketDTO, $sessionId);

        $this->basketRepository->save($basket);

        return $basket;
    }

    /**
     * @param int $basketId
     */
    public function deleteBasketItem(int $basketId): void
    {
        $basket = $this->basketRepository->findById($basketId);
        if ($basket) {
            $this->basketRepository->delete($basket);
        }
    }
}