Pre Order Application
=======================




Start Project
---------------------------

This application provides a `docker-compose.yml` for use with
[docker-compose](https://docs.docker.com/compose/); it
uses the `Dockerfile` provided as its base. Build and start the image using:

```bash
$ docker-compose up -d --build
```

Auth Information
-------

Username: admin
Password:adminpassword


Services
---------------------------
**PreOrder List:**
``` 
URL: /api/preorders

TYPE: GET
```
**PreOrder Create:**

```
URL: /api/preorder

TYPE: POST

AUTH: Required

PARAMETER: {"name" : "Ahmet", "surname" : "Mehmet","phone" : "1111", "email" : "a@a.com"}
```

**PreOrder Update:**
```
URL: /api/preorder/{preOrderId}

TYPE: PUT

PARAMETER: {"name" : "Ahmet", "surname" : "Mehmet","phone" : "1111", "email" : "a@a.com"}
```

**PreOrder Status Update**
```
URL: /api/preorder/updateStatus/{preOrderId}

TYPE: PUT

AUTH: Required

PARAMETER: {"status" : "approved"}
```

**PreOrder Delete**
```
URL: /api/preorder/{preOrderId}

TYPE: DELETE

AUTH: Required
```


**Basket List**
```
URL: /api/basket

TYPE: GET

```

**Basket Item Add**
```
URL: /api/basket

TYPE: POST

PARAMETER: {"product_id" : 1, "quantity" : 1 } 

```

**Basket Item Update**
```
URL: /api/basket/{basketId}

TYPE: PUT

PARAMETER: {"product_id" : 1, "quantity" : 1 } 

```

**Basket Item Delete**
```
URL: /api/basket/{basketId}

TYPE: DELETE
```




Command
---------------------------
AutoRejected Command

```bash
$ php bin/console preorder:autorejected
```