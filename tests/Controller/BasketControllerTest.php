<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BasketControllerTest extends WebTestCase
{
    public function testGetBasket()
    {
        $client = static::createClient();

        $client->request('GET', '/api/basket');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testPostBasket()
    {
        $client = static::createClient();

        $client->request('POST', '/api/basket', [], [], ['CONTENT_TYPE' => 'application/json'],
            '{"product_id" : 1,"quantity" : 1}');


        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testPutBasket()
    {
        $client = static::createClient();

        $client->request('GET', '/api/basket');

        $basket = json_decode($client->getResponse()->getContent());

        foreach ($basket as $item) {
            $client->request('PUT', '/api/basket/' . $item->id, [], [], ['CONTENT_TYPE' => 'application/json'],
                '{"product_id" : 1,"quantity" : 2}');


            $this->assertEquals(200, $client->getResponse()->getStatusCode());
        }
    }

    public function testDeleteBasket()
    {
        $client = static::createClient();

        $client->request('GET', '/api/basket');

        $basket = json_decode($client->getResponse()->getContent());

        foreach ($basket as $item) {
            $client->request('DELETE', '/api/basket/' . $item->id);

            $this->assertEquals(204, $client->getResponse()->getStatusCode());
        }
    }
}