<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PreOrderControllerTest extends WebTestCase
{

    public function testGetPreOrders()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'adminpassword'
        ]);

        $client->request('GET', '/api/preorders');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testPostPreOrder()
    {
        $client = static::createClient();

        $client->request('POST', '/api/basket', [], [], ['CONTENT_TYPE' => 'application/json'],
            '{"product_id" : 1,"quantity" : 2}');

        $client->request('POST', '/api/preorder', [], [], ['CONTENT_TYPE' => 'application/json'],
            '{"name":"Selim","surname":"Saral","phone":"5378109336","email":"saral_selim@hotmail.com"}');

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }


    public function testPutPreOrder()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'adminpassword'
        ]);

        $client->request('GET', '/api/preorders');

        $preOrders = json_decode($client->getResponse()->getContent());

        foreach ($preOrders as $item) {

            $client->request('PUT', '/api/preorder/' . $item->id, [], [], ['CONTENT_TYPE' => 'application/json'],
                '{"name":"Selim - Updated","surname":"Saral","phone":"5378109336","email":"saral_selim@hotmail.com"}');

            $this->assertEquals(200, $client->getResponse()->getStatusCode());
        }

    }

    public function testChangeStatus()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'adminpassword'
        ]);

        $client->request('GET', '/api/preorders');

        $preOrders = json_decode($client->getResponse()->getContent());

        foreach ($preOrders as $item) {

            $client->request('PUT', '/api/preorder/updateStatus/' . $item->id, [], [],
                ['CONTENT_TYPE' => 'application/json'],
                '{"status" : "approved"}');

            $this->assertEquals(200, $client->getResponse()->getStatusCode());
        }
    }

    public function testDeletePreOrder()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'adminpassword'
        ]);

        $client->request('GET', '/api/preorders');

        $preOrders = json_decode($client->getResponse()->getContent());

        foreach ($preOrders as $item) {

            $client->request('DELETE', '/api/preorder/' . $item->id);

            $this->assertEquals(204, $client->getResponse()->getStatusCode());
        }

    }
}